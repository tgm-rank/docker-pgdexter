FROM debian:stretch

RUN set -ex \\
    \
    && apt update && apt install -y curl build-essential wget apt-transport-https git \
    && curl -L https://dl.packager.io/srv/pghero/dexter/key | apt-key add - \
    && wget -O /etc/apt/sources.list.d/dexter.list https://dl.packager.io/srv/pghero/dexter/master/installer/debian/9.repo \
    && apt-get update \
    && apt-get install -y dexter
